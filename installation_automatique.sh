#!/bin/bash
setip() {
    sed -i "2s/.*/ansible_host: $1/" host_vars/*.yml
}

testconnectivity() {
    if [ "$1" = "user" ]; then
        ansible all -i inventory.yml -m ping -v -e 'ansible_user=julien'
    else
        ansible all -i inventory.yml -m ping -v -e 'ansible_user=root'
    fi
    if [ "$?" -eq 0 ]; then
        co="0"
    else
        co="1"
    fi
    
}

userspace() {
    testconnectivity user
    echo $co
    if [ "$co" = "0" ]; then
        echo "Connexion en tant qu'utilisateur réussie"
    else
        read -n 1 -p "Impossible de se connecter, changer l'IP ? [y/n]" yn
        if [ "$yn" = "y" ] || [ "$yn" = "Y" ]; then
            read -p "Nouvelle IP : " ip
            setip $ip
        fi
    fi
    ansible-playbook --ask-vault-pass -i inventory.yml play.yml --tag userspace
    if [ "$?" -eq 0 ]; then
        echo "Installation terminée"
        read -n 1 -pas "Voulez-vous lancer les tests ? [y/n]" yn
        if [ "$yn" = "y" ] || [ "$yn" = "Y" ]; then
            ansible-playbook -i inventory.yml play.yml --tag tests
        fi
    else
        echo "Erreur lors de l'installation"
    fi
    exit
}
if [ "$1" = "-u" ]; then
    userspace
fi
export PATH="~/.local/bin:$PATH"
ansv=$(ansible --version)
if [ $? -ne 0 ]
then
    yn='n'
    read -n 1 -p "Ansible n'est pas installé !" yn
    if [ "$yn" = "y" ]
    then 
        pip install ansible
    fi
fi
yn="y"
read -n 1 -p "La cible est-elle démarré sur l'ISO Arch Linux et init.sh a-t-il été lancé ? [Y/n]" yn
echo
if [ "$yn" = "n" ]
then 
    echo "Lancez la cible sur l'ISO et relancez le script !"    
    exit 1
fi
read -p "Entrez l'IP de la cible : " ip
read -p "Entrez le hostname qui sera donné à la cible : " hostname
mv host_vars/*.yml host_vars/$hostname.yml
sed -i "3s/.*/    $hostname:/" inventory.yml
setip $ip
echo -n '-------------------------------------'
cat group_vars/all.yml
echo
echo '----------------------------------------'
yn="y"
read -n 1 -p "Ces variables sont-elles correctes ? [Y/n]" yn
echo
if [ "$yn" = "n" ]
then
    nano group_vars/all.yml
fi
echo "Attention ! Cette installation va supprimer tous les fichiers du disque dur sélectionné comme disque d'installation, appuyez sur CTRL+C pour annuler"
read -n 1 -p "Appuyez sur une touche pour continuer"
echo "\nTest de connection à $hostname -> $ip"
connection=0
while [ $connection -eq 0 ]
do
    if [ testconnectivity 0 -ne 0 ]
    then
        read -n 1 -p "Connection impossible à la cible, réessayer ? [Y/n]" yn
        echo
        if [ "$yn" = 'n' ]; then exit; fi
    else
        connection=1
    fi
done
ansible-playbook -i inventory.yml play.yml --tag install_base
if [ $? -ne 0 ]
then
    yn="y"
    read -n 1 -p "La cible a-t-elle redémarrée ? [Y/n]" yn
    if [ "$yn" = "n" ]
    then
        echo "Une erreur est survenue pendant l'installation..."
        echo "Si l'installation semble avoir été correctement effectuée, relancez le script avec l'argument -u"
        exit 1
    fi
    echo "L'installation va se poursuivre normalement..."
    userspace
fi
echo "L'installation est terminée !"