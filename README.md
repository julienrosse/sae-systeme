# SAE-systeme 09/22

## Introduction
Ce projet est ma SAE de BUT en système. Il consiste à mettre en place un environnement de développement Linux pour qu'il permette ensuite une productivité acrue.
Il utilise :
- Arch Linux
- KDE
- Ansible

## Installation
1. Installer Ansible sur une machine (dénommé "Controlleur Ansible") qui dispose d'un accès réseau à la cible du déploiement
```bash
pip install ansible
```
2. Télécharger l'ISO Arch Linux depuis un des mirroirs
3. Flasher l'ISO sur une clé USB
    - De manière "classique"
    - En utilisant Ventoy, dans ce cas, copier aussi le script `init.sh`
4. Démarrer sur l'environnement live Arch
5. **Si vous avez utilisé la methode classique**, téléchargez `init.sh` en utilisant la 1ère commande si dessous
```bash
curl https://gitlab.com/julienrosse/sae-systeme/-/raw/main/init.sh -o init.sh
```
6. Lancer le script
```bash
bash init.sh
```
L'adresse IP de votre machine s'affiche

## Installation automatique 
Sur le controlleur Ansible, lancer 
```bash
installation_automatique.sh
```
Si le script reste bloqué sur `[Reboot]`, alors que la cible a redémarré, interrompre avec Ctrl+C
## Usage manuel d'Ansible
1. Sur le controlleur Ansible, se placer dans le dossier du dépot

2. Renommer le fichier `vm.yml` situé dans host_vars en `<hostname>.yml`, remplacant \<hostname\> par le nom souhaité de la machine.

3. Modifier `inventory.yml` et remplacer vm: par \<hostname\>:

4. Executer la commande suivante:
```bash
ansible-playbook -i inventory.yml play.yml
```
5. Attendre la demande de redemmarage

Si il y a une **erreur de connection** à la machine durant la seconde étape, s'assurer que l'IP de la machine n'a pas changé lors du redemmarage. Si c'est le cas, modifier l'IP dans hosts_vars/\<hostname\>.yml puis reprendre l'installation avec
```bash
ansible-playbook -i inventory.yml play.yml --tags userspace
```
