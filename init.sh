#!/bin/bash
loadkeys fr
passwd -d root
sed -i 's/#PermitEmptyPasswords no/PermitEmptyPasswords yes/' /etc/ssh/sshd_config
ip ad
systemctl restart sshd
